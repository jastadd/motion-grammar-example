package de.tudresden.inf.st.mg.common;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ValueNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.tudresden.inf.st.mg.jastadd.model.Bin;
import de.tudresden.inf.st.mg.jastadd.model.MovableObject;
import de.tudresden.inf.st.mg.jastadd.model.Robot;
import de.tudresden.inf.st.mg.jastadd.model.Table;

import java.io.IOException;

public class ColorDeserializer extends JsonDeserializer<String> {
    @Override
    public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode node = p.getCodec().readTree(p);
        JsonNode nodeR = node.get("r");
        Long r = nodeR == null ? 0 : Math.round(nodeR.asDouble() * 255);
        JsonNode nodeG = node.get("g");
        Long g = nodeG == null ? 0 : Math.round(nodeG.asDouble() * 255);
        JsonNode nodeB = node.get("b");
        Long b = nodeB == null ? 0 : Math.round(nodeB.asDouble() * 255);
        return "#" + String.format("%02x%02x%02x", r, g, b);
    }
}
