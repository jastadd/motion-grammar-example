package de.tudresden.inf.st.mg;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.tudresden.inf.st.mg.common.MotionGrammarConfig;
import de.tudresden.inf.st.mg.common.MotionGrammarParser;
import de.tudresden.inf.st.mg.common.TableDeserializer;
import de.tudresden.inf.st.mg.common.Webserver;
import de.tudresden.inf.st.mg.jastadd.model.JastAddList;
import de.tudresden.inf.st.mg.jastadd.model.RobotWorld;
import de.tudresden.inf.st.mg.jastadd.model.Table;
import de.tudresden.inf.st.mg.jastadd.model.World;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Random;

public class ImmersiveSortingController {

  public static final Path TIDY_AST_DIAGRAM_DIR = Path.of("src", "gen", "resources", "diagrams", "parsing", "tidy");
  private static final Logger logger = LoggerFactory.getLogger(ImmersiveSortingController.class);
  private final RobotWorld world;
  private ParseThread parse;

  public ImmersiveSortingController(boolean simulate) {
    MotionGrammarConfig.astDiagramDir = TIDY_AST_DIAGRAM_DIR;
    try {
      Files.walk(TIDY_AST_DIAGRAM_DIR).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
      Files.createDirectories(TIDY_AST_DIAGRAM_DIR);
    } catch (IOException ignored) {
    }

    if (simulate) {
      world = RobotWorld.initialWorld(new Random(1));
    } else {
      world = RobotWorld.initialWorld();
    }
    try {
      SimpleModule module = new SimpleModule();
      module.addDeserializer(Table.class, new TableDeserializer());
      ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
      mapper.registerModule(module);
      world.setDemonstrationTable(mapper.readValue(ImmersiveSortingController.class.getResourceAsStream("config_scene_virtual-table.yaml"), Table.class));
    } catch (IOException e) {
      e.printStackTrace();
    }

    // initialize webserver
    Webserver ws = Webserver.getInstance();

    ws.setCallback("cell1", (String command) -> {
      logger.info("got message " + command);
      if ("parse".equals(command)) {
        startParse();
      } else if ("reset".equals(command)) {
        resetSelections();
        printContext("reset selection");
      }
    });


    World.enableContextPrinting(true);
    world.printContext("initial");

    if (!simulate) {
      org.fusesource.mqtt.client.MQTT handler = new org.fusesource.mqtt.client.MQTT();
      try {
        handler.setHost("tcp://localhost:1883");
      } catch (URISyntaxException e) {
        e.printStackTrace();
      }
      world.connection = handler.blockingConnection();
      try {
        world.connection.connect();
      } catch (Exception e) {
        e.printStackTrace();
      }

      System.err.println(world.connection.isConnected());

      try {
        world.connectTable("mqtt://localhost//ceti_cell_1/scene/update");
      } catch (IOException e) {
        e.printStackTrace();
      }

      try {
        world.connectSelection("mqtt://localhost/vr_selection");
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }

  /**
   * Runs the parser on the actual robot. For this to work, a robot controller must be connected to an MQTT server running
   * on localhost:1883
   */
  public static void main(String[] args) {
    ImmersiveSortingController controller = new ImmersiveSortingController(false);
    try {
      Thread.sleep(Long.MAX_VALUE);
    } catch (InterruptedException ignored) {
    }
  }

  public void startParse() {
    if (parse != null && parse.isAlive()) {
      parse.stopParsing();
      try {
        parse.join();
      } catch (InterruptedException ignored) {
      }
    }
    parse = new ParseThread();
    parse.start();
  }

  public void resetSelections() {
    world.setSelectionList(new JastAddList<>());
  }

  public void printContext(String message) {
    world.printContext(message);
  }

  class ParseThread extends Thread {

    private RobotParser parser;

    @Override
    public void run() {
      logger.info("starting to parse");
      // create a parser using the world
      parser = new RobotParser(world);

      // parse (synchronously, long-running)
      try {
        parser.parse();
        logger.info("parsing completed!");
      } catch (MotionGrammarParser.ParseException e) {
        throw new RuntimeException(e);
      } catch (MotionGrammarParser.ParsingStoppedException ignored) {
        logger.info("Parsing stopped");
      }
    }

    void stopParsing() {
      parser.setShouldStopParsing(true);
    }
  }

}
