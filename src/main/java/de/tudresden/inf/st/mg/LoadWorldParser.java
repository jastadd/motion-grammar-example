package de.tudresden.inf.st.mg;

import de.tudresden.inf.st.mg.common.MotionGrammarParser;
import de.tudresden.inf.st.mg.jastadd.model.*;


public final class LoadWorldParser extends MotionGrammarParser<T> {

  private static final String WORLD = "World";

  public LoadWorldParser(World world) {
    contexts_.put(WORLD, world);
  }

  private World getWorld() {
    return (World) contexts_.get(WORLD);
  }

  @Override
  protected void parse(ASTNode<?> parent, int index) throws ParseException {
    parseT(parent, index);
  }

  private Load peekedLoad_ = null;

  private boolean peekLoad() {
    peekedLoad_ = getWorld().parseLoad();
    return peekedLoad_ != null;
  }

  private void parseLoad(ASTNode<?> parent, int index) throws ParseException {
    Load result;

    if (peekedLoad_ != null) {
      result = peekedLoad_;
      peekedLoad_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseLoad();
      if (result == null) {
        throw new ParseException(Load.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for Load
    result.action(getWorld());
    printAST("parseLoad", result);
  }

  private Unload peekedUnload_ = null;

  private boolean peekUnload() {
    peekedUnload_ = getWorld().parseUnload();
    return peekedUnload_ != null;
  }

  private void parseUnload(ASTNode<?> parent, int index) throws ParseException {
    Unload result;

    if (peekedUnload_ != null) {
      result = peekedUnload_;
      peekedUnload_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseUnload();
      if (result == null) {
        throw new ParseException(Unload.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for Unload
    result.action(getWorld());
    printAST("parseUnload", result);
  }

  private Full peekedFull_ = null;

  private boolean peekFull() {
    peekedFull_ = getWorld().parseFull();
    return peekedFull_ != null;
  }

  private void parseFull(ASTNode<?> parent, int index) throws ParseException {
    Full result;

    if (peekedFull_ != null) {
      result = peekedFull_;
      peekedFull_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseFull();
      if (result == null) {
        throw new ParseException(Full.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for Full
    result.action(getWorld());
    printAST("parseFull", result);
  }

  private void parseT(ASTNode<?> parent, int index) throws ParseException {
    T result;

    // try the different types of T
    if (peekFull()) {
      result = new T2();
      result.parser = this;
      parent.setChild(result, index);
      parseT2(parent, index);
    } else if (peekLoad()) {
      result = new T1();
      result.parser = this;
      parent.setChild(result, index);
      parseT1(parent, index);
    } else {
      throw new ParseException("T", Full.type(), Load.type());
    }


    // semantic action for T
    result.action(getWorld());
    printAST("parseT", result);
  }

  private void parseT1(ASTNode<?> parent, int index) throws ParseException {
    T1 result = (T1) parent.getChild(index);

    parseLoad(result, 0);
    parseT(result, 1);
    parseUnload(result, 2);

    // semantic action for T1
    result.action(getWorld());
    printAST("parseT1", result);
  }

  private void parseT2(ASTNode<?> parent, int index) throws ParseException {
    T2 result = (T2) parent.getChild(index);

    parseFull(result, 0);

    // semantic action for T2
    result.action(getWorld());
    printAST("parseT2", result);
  }

}
