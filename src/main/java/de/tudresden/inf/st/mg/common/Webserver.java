package de.tudresden.inf.st.mg.common;

import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import io.javalin.websocket.WsContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

public class Webserver {

  private static final Logger logger = LoggerFactory.getLogger(Webserver.class);
  private final Map<WsContext, String> clients = new ConcurrentHashMap<>();
  private static Webserver INSTANCE;
  private Diagram ast;
  private Diagram context;

  private final Map<String, Consumer<String>> callbacks = new ConcurrentHashMap<>();

  private Webserver() {
    ast = new Diagram(new Date(), 0, "<no rule>", "", "ast");
    context = new Diagram(Date.from(Instant.now()), 0, "<no rule>", "", "context");

    Javalin webserver = Javalin.create(JavalinConfig::enableCorsForAllOrigins).start(7070);
    webserver.get("/", ctx -> ctx.result("the context is delivered at /context and the ast is delivered at /ast"));

    webserver.get("/ast/", ctx -> {
      ctx.json(ast);
    });

    webserver.get("/context/", ctx -> {
      ctx.json(context);
    });

    webserver.ws("ast-events", ws -> {
      ws.onConnect(ctx -> {
        System.err.println("somebody connected " + ctx.host());
        clients.put(ctx, ctx.host());
      });
      ws.onClose(ctx -> {
        System.err.println("somebody disconnected");
        clients.remove(ctx);
      });
      ws.onMessage(ctx -> {
        String[] splitString = ctx.message().split("\\s");
        if (splitString.length == 2) {
          if (callbacks.containsKey(splitString[0])) {
            callbacks.get(splitString[0]).accept(splitString[1]);
          }
        } else {
          logger.error("Unable to execute command, because it does not fit the command syntax: " + ctx.message());
        }
      });
    });

  }

  public static Webserver getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new Webserver();
    }
    return INSTANCE;
  }

  public void setCallback(String client, Consumer<String> callback) {
    callbacks.put(client, callback);
  }

  public void publish(Date timestamp, int step, String parseRule, Path diagramPath, String type) {
    try {
      if ("ast".equals(type)) {
        ast = new Diagram(timestamp, step, parseRule, Files.readString(diagramPath), type);
        clients.keySet().forEach(session -> session.send(ast));
      } else if ("context".equals(type)) {
        context = new Diagram(timestamp, step, parseRule, Files.readString(diagramPath), type);
        clients.keySet().forEach(session -> session.send(context));
      }
    } catch (IOException e) {
      System.err.println("Unable to read " + type + " diagram file " + diagramPath);
      e.printStackTrace();
    }
  }


  public static class Diagram {
    public Date timestamp;
    public int step;
    public String parseRule;
    public String diagram;

    public String type;

    public Diagram(Date timestamp, int step, String parseRule, String diagram, String type) {
      this.timestamp = timestamp;
      this.step = step;
      this.parseRule = parseRule;
      this.diagram = diagram;
      this.type = type;
    }
  }

}
