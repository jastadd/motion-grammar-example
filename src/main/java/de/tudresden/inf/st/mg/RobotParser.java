package de.tudresden.inf.st.mg;

import de.tudresden.inf.st.mg.common.MotionGrammarParser;
import de.tudresden.inf.st.mg.jastadd.model.*;


public final class RobotParser extends MotionGrammarParser<Tidy> {

  private static final String WORLD = "World";

  public RobotParser(World world) {
    contexts_.put(WORLD, world);
  }

  private World getWorld() {
    return (World) contexts_.get(WORLD);
  }

  @Override
  protected void parse(ASTNode<?> parent, int index) throws ParseException {
    parseTidy(parent, index);
  }

  private EmptyTable peekedEmptyTable_ = null;

  private boolean peekEmptyTable() {
    peekedEmptyTable_ = getWorld().parseEmptyTable();
    return peekedEmptyTable_ != null;
  }

  private void parseEmptyTable(ASTNode<?> parent, int index) throws ParseException {
    EmptyTable result;

    if (peekedEmptyTable_ != null) {
      result = peekedEmptyTable_;
      peekedEmptyTable_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseEmptyTable();
      if (result == null) {
        throw new ParseException(EmptyTable.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for EmptyTable
    result.action(getWorld());
    printAST("parseEmptyTable", result);
  }

  private volatile NotEmptyTable peekedNotEmptyTable_ = null;

  private boolean peekNotEmptyTable() {
    peekedNotEmptyTable_ = getWorld().parseNotEmptyTable();
    return peekedNotEmptyTable_ != null;
  }

  private void parseNotEmptyTable(ASTNode<?> parent, int index) throws ParseException {
    NotEmptyTable result;

    if (peekedNotEmptyTable_ != null) {
      result = peekedNotEmptyTable_;
      peekedNotEmptyTable_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseNotEmptyTable();
      if (result == null) {
        throw new ParseException(NotEmptyTable.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for NotEmptyTable
    result.action(getWorld());
    printAST("parseNotEmptyTable", result);
  }

  private AvailableObject peekedAvailableObject_ = null;

  private boolean peekAvailableObject() {
    peekedAvailableObject_ = getWorld().parseAvailableObject();
    return peekedAvailableObject_ != null;
  }

  private void parseAvailableObject(ASTNode<?> parent, int index, Tidy context) throws ParseException {
    AvailableObject result;

    if (peekedAvailableObject_ != null) {
      result = peekedAvailableObject_;
      peekedAvailableObject_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseAvailableObject();
      if (result == null) {
        throw new ParseException(AvailableObject.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for ObjectAtWrongPlace
    result.action(getWorld());
    printAST("parseAvailableObject", result);
  }

  private RobotIsReadyToPickToken peekedRobotIsReadyToPickToken_ = null;

  private boolean peekRobotIsReadyToPickToken() {
    peekedRobotIsReadyToPickToken_ = getWorld().parseRobotIsReadyToPickToken();
    return peekedRobotIsReadyToPickToken_ != null;
  }

  private void parseRobotIsReadyToPickToken(ASTNode<?> parent, int index) throws ParseException {
    RobotIsReadyToPickToken result;

    if (peekedRobotIsReadyToPickToken_ != null) {
      result = peekedRobotIsReadyToPickToken_;
      peekedRobotIsReadyToPickToken_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseRobotIsReadyToPickToken();
      if (result == null) {
        throw new ParseException(RobotIsReadyToPickToken.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for RobotIsReadyToPickToken
    result.action(getWorld());
    printAST("parseRobotIsReadyToPickToken", result);
  }

  private RobotIsNotReadyToPickToken peekedRobotIsNotReadyToPickToken_ = null;

  private boolean peekRobotIsNotReadyToPickToken() {
    peekedRobotIsNotReadyToPickToken_ = getWorld().parseRobotIsNotReadyToPickToken();
    return peekedRobotIsNotReadyToPickToken_ != null;
  }

  private void parseRobotIsNotReadyToPickToken(ASTNode<?> parent, int index) throws ParseException {
    RobotIsNotReadyToPickToken result;

    if (peekedRobotIsNotReadyToPickToken_ != null) {
      result = peekedRobotIsNotReadyToPickToken_;
      peekedRobotIsNotReadyToPickToken_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseRobotIsNotReadyToPickToken();
      if (result == null) {
        throw new ParseException(RobotIsNotReadyToPickToken.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for RobotIsNotReadyToPickToken
    result.action(getWorld());
    printAST("parseRobotIsNotReadyToPickToken", result);
  }

  private RobotIsReadyToDropToken peekedRobotIsReadyToDropToken_ = null;

  private boolean peekRobotIsReadyToDropToken() {
    peekedRobotIsReadyToDropToken_ = getWorld().parseRobotIsReadyToDropToken();
    return peekedRobotIsReadyToDropToken_ != null;
  }

  private void parseRobotIsReadyToDropToken(ASTNode<?> parent, int index) throws ParseException {
    RobotIsReadyToDropToken result;

    if (peekedRobotIsReadyToDropToken_ != null) {
      result = peekedRobotIsReadyToDropToken_;
      peekedRobotIsReadyToDropToken_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseRobotIsReadyToDropToken();
      if (result == null) {
        throw new ParseException(RobotIsReadyToDropToken.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for RobotIsReadyToDropToken
    result.action(getWorld());
    printAST("parseRobotIsReadyToDropToken", result);
  }

  private RobotIsNotReadyToDropToken peekedRobotIsNotReadyToDropToken_ = null;

  private boolean peekRobotIsNotReadyToDropToken() {
    peekedRobotIsNotReadyToDropToken_ = getWorld().parseRobotIsNotReadyToDropToken();
    return peekedRobotIsNotReadyToDropToken_ != null;
  }

  private void parseRobotIsNotReadyToDropToken(ASTNode<?> parent, int index) throws ParseException {
    RobotIsNotReadyToDropToken result;

    if (peekedRobotIsNotReadyToDropToken_ != null) {
      result = peekedRobotIsNotReadyToDropToken_;
      peekedRobotIsNotReadyToDropToken_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseRobotIsNotReadyToDropToken();
      if (result == null) {
        throw new ParseException(RobotIsNotReadyToDropToken.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for RobotIsNotReadyToDropToken
    result.action(getWorld());
    printAST("parseRobotIsNotReadyToDropToken", result);
  }

  private RobotIsIdle peekedRobotIsIdle_ = null;

  private boolean peekRobotIsIdle() {
    peekedRobotIsIdle_ = getWorld().parseRobotIsIdle();
    return peekedRobotIsIdle_ != null;
  }

  private void parseRobotIsIdle(ASTNode<?> parent, int index) throws ParseException {
    RobotIsIdle result;

    if (peekedRobotIsIdle_ != null) {
      result = peekedRobotIsIdle_;
      peekedRobotIsIdle_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseRobotIsIdle();
      if (result == null) {
        throw new ParseException(RobotIsIdle.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for RobotIsIdle
    result.action(getWorld());
    printAST("parseRobotIsIdle", result);
  }

  private RobotIsNotIdle peekedRobotIsNotIdle_ = null;

  private boolean peekRobotIsNotIdle() {
    peekedRobotIsNotIdle_ = getWorld().parseRobotIsNotIdle();
    return peekedRobotIsNotIdle_ != null;
  }

  private void parseRobotIsNotIdle(ASTNode<?> parent, int index) throws ParseException {
    RobotIsNotIdle result;

    if (peekedRobotIsNotIdle_ != null) {
      result = peekedRobotIsNotIdle_;
      peekedRobotIsNotIdle_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseRobotIsNotIdle();
      if (result == null) {
        throw new ParseException(RobotIsNotIdle.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for RobotIsNotIdle
    result.action(getWorld());
    printAST("parseRobotIsNotIdle", result);
  }

  private RobotHasItemAttached peekedRobotHasItemAttached_ = null;

  private boolean peekRobotHasItemAttached() {
    peekedRobotHasItemAttached_ = getWorld().parseRobotHasItemAttached();
    return peekedRobotHasItemAttached_ != null;
  }

  private void parseRobotHasItemAttached(ASTNode<?> parent, int index) throws ParseException {
    RobotHasItemAttached result;

    if (peekedRobotHasItemAttached_ != null) {
      result = peekedRobotHasItemAttached_;
      peekedRobotHasItemAttached_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseRobotHasItemAttached();
      if (result == null) {
        throw new ParseException(RobotHasItemAttached.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for RobotHasItemAttached
    result.action(getWorld());
    printAST("parseRobotHasItemAttached", result);
  }

  private RobotHasNoItemAttached peekedRobotHasNoItemAttached_ = null;

  private boolean peekRobotHasNoItemAttached() {
    peekedRobotHasNoItemAttached_ = getWorld().parseRobotHasNoItemAttached();
    return peekedRobotHasNoItemAttached_ != null;
  }

  private void parseRobotHasNoItemAttached(ASTNode<?> parent, int index) throws ParseException {
    RobotHasNoItemAttached result;

    if (peekedRobotHasNoItemAttached_ != null) {
      result = peekedRobotHasNoItemAttached_;
      peekedRobotHasNoItemAttached_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseRobotHasNoItemAttached();
      if (result == null) {
        throw new ParseException(RobotHasNoItemAttached.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for RobotHasNoItemAttached
    result.action(getWorld());
    printAST("parseRobotHasNoItemAttached", result);
  }

  private void parseRightPlace(ASTNode<?> parent, int index) throws ParseException {
    RightPlace result;

    result = getWorld().parseRightPlace(parent.object());
    if (result == null) {
      throw new ParseException(RightPlace.type());
    }

    parent.setChild(result, index);

    // semantic action for RightPlace
    result.action(getWorld());
    printAST("parseRightPlace", result);
  }

  private MisplacedObject peekedMisplacedObject_ = null;
  private boolean peekMisplacedObject() {
    peekedMisplacedObject_ = getWorld().parseMisplacedObject();
    return peekedMisplacedObject_ != null;
  }

  private void parseMisplacedObject(ASTNode<?> parent, int index) throws ParseException {
    MisplacedObject result;

    if (peekedMisplacedObject_ != null) {
      result = peekedMisplacedObject_;
      peekedMisplacedObject_ = null; // TODO check if all peeked values are actually parsed afterwards
    } else {
      result = getWorld().parseMisplacedObject();
      if (result == null) {
        throw new ParseException(MisplacedObject.type());
      }
    }

    parent.setChild(result, index);

    // semantic action for MisplacedObject
    result.action(getWorld());
    printAST("parseMisplacedObject", result);
  }

  private void parseTidy(ASTNode<?> parent, int index) throws ParseException {
    Tidy result = new Tidy();
    result.parser = this;
    parent.setChild(result, index);

    peekNotEmptyTable();
    while (peekedNotEmptyTable_ == null) {
      waitSomeTime();
      peekNotEmptyTable();
    }
    parseNotEmptyTable(result, 0);

    while (true) {
      peekMisplacedObject();
      if (peekedMisplacedObject_ != null) {
        int i = result.getNumMoveObjectToCorrectPlace();
        result.addMoveObjectToCorrectPlace(null);
        parseMoveObjectToCorrectPlace(result.getMoveObjectToCorrectPlaceList(), i);
      } else {
        peekEmptyTable();
        if (peekedEmptyTable_ != null) {
          break;
        } else {
          waitSomeTime();
        }
      }
    }

    parseEmptyTable(result, 2);

    // semantic action for Tidy
    result.action(getWorld());
    printAST("parseTidy", result);
  }

  private void parseMoveObjectToCorrectPlace(ASTNode<?> parent, int index) throws ParseException {
    MoveObjectToCorrectPlace result = new MoveObjectToCorrectPlace();
    result.parser = this;
    parent.setChild(result, index);

    parseMisplacedObject(result, 0);
    parsePickUpObject(result, 1);
    parseDropObjectAtRightPlace(result, 2);
    peekRobotIsReadyToPickToken();
    while (peekedRobotIsReadyToPickToken_ == null) {
      waitSomeTime();
      peekRobotIsReadyToPickToken();
    }
    parseRobotIsReadyToPickToken(result, 3);

    // semantic action for MoveObjectToCorrectPlace
    result.action(getWorld());
    printAST("parseMoveObjectToCorrectPlace", result);
  }

  private void parseDropObjectAtRightPlace(ASTNode<?> parent, int index) throws ParseException {
    DropObjectAtRightPlace result = new DropObjectAtRightPlace();
    result.parser = this;
    parent.setChild(result, index);


    peekRobotIsReadyToDropToken();
    while (peekedRobotIsReadyToDropToken_ == null) {
      waitSomeTime();
      peekRobotIsReadyToDropToken();
    }
    parseRobotIsReadyToDropToken(result, 0);

    // semantic action for DropObjectAtRightPlace
    result.action(getWorld());
    printAST("parseDropObjectAtRightPlace", result);
  }

  private void parsePickUpObject(ASTNode<?> parent, int index) throws ParseException {
    PickUpObject result = new PickUpObject();
    result.parser = this;
    parent.setChild(result, index);

    // semantic action for PickUpObject
    result.action(getWorld());
    printAST("parsePickUpObject", result);
  }

}
