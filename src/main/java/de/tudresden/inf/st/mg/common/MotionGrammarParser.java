package de.tudresden.inf.st.mg.common;

import de.tudresden.inf.st.jastadd.dumpAst.ast.Dumper;
import de.tudresden.inf.st.jastadd.dumpAst.ast.SkinParamBooleanSetting;
import de.tudresden.inf.st.jastadd.dumpAst.ast.SkinParamStringSetting;
import de.tudresden.inf.st.jastadd.dumpAst.ast.TransformationException;
import de.tudresden.inf.st.mg.jastadd.model.*;

import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public abstract class MotionGrammarParser<T extends MotionGrammarElement> {

  protected final Map<String, ASTNode<?>> contexts_ = new HashMap<>();
  protected ASTNode<T> rootContainer_;
  private int timeStep_;

  private boolean shouldStopParsing = false;

  public void setShouldStopParsing(boolean value) {
    shouldStopParsing = value;
  }

  protected void waitSomeTime() {
    if (shouldStopParsing) {
      throw new ParsingStoppedException();
    }
    try {
      TimeUnit.MILLISECONDS.sleep(100);
    } catch (InterruptedException ignored) {
    }
  }

  public static void setDebugDiagramDir(Path p) {
    MotionGrammarConfig.astDiagramDir = p;
  }

  /**
   * Prints the AST and all context models into PlantUML diagrams. Each time this method is called, a counter incremented.
   *
   * @param step          a name for the current step, which is added to the file name
   * @param highlightNode a node that is highlighted, to illustrate what happens in the current step; may be null
   */
  protected void printAST(String step, ASTNode<?> highlightNode) {

    if (MotionGrammarConfig.astDiagramDir == null) {
      return;
    }

    final String N_N = "9594f7";
    final String H_N = "d4d4fc";
    final String N_T = "c17dff";
    final String H_T = "ddbefa";

    try {
      Path svgPath = MotionGrammarConfig.astDiagramDir.resolve("AST-" + String.format("%03d", timeStep_) + "-" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSS").format(new java.util.Date()) + "-" + step + ".svg");
      Dumper.read(rootContainer_.getChild(0))
          .setBackgroundColorMethod(n -> (n == highlightNode ? (n instanceof Token ? H_T : H_N) : (n instanceof Token ? N_T : N_N)))
          .includeChildWhen((parentNode, childNode, contextName) -> !(parentNode instanceof Token))
          .setNameMethod(o -> {
            String result = "";
            try {
              o.getClass().getDeclaredMethod("action", World.class);
              result += "<:boom:> ";
            } catch (NoSuchMethodException ignored) {
            }
            if (o instanceof Token) {
              Token t = (Token) o;
              // every token with optional children has at least one (in the OR-case)
              result += t.label();
            } else {
              result += o.getClass().getSimpleName();
            }
            return result;
          })
          .customPreamble("title " + step)
          .skinParam(SkinParamBooleanSetting.Shadowing, false)
          .skinParam(SkinParamStringSetting.backgroundColor, "white")
          .dumpAsSVG(svgPath);
      Webserver.getInstance().publish(new Date(), timeStep_, step, svgPath, "ast");
      timeStep_++;
    } catch (IOException | TransformationException e) {
      e.printStackTrace();
    }
  }

  /**
   * Parse a motion grammar with root T
   *
   * @return the parsed AST of type T
   */
  public T parse() throws ParseException {
    // don't try this at home
    rootContainer_ = new ASTNode<>();
    if (rootContainer_.getNumChild() == 0) {
      rootContainer_.addChild(null);
    }
    printAST("initial", null);
    parse(rootContainer_, 0);
    T result = rootContainer_.getChild(0);
    printAST("complete", null);
    result.setParent(null);
    rootContainer_.setChild(null, 0);
    return result;
  }

  protected abstract void parse(ASTNode<?> parent, int index) throws ParseException;

  public static class ParseException extends Exception {
    public ParseException(String ruleName, TokenType... expectedTokens) {
      super("Unable to parse nonterminal " + ruleName + ". Expected tokens " + Arrays.stream(expectedTokens).map(Object::toString).collect(Collectors.joining(", ")) + ".");
    }

    public ParseException(TokenType expectedToken) {
      super("Unable to parse token " + expectedToken.name() + ".");
    }
  }

  public static class ParsingStoppedException extends RuntimeException {
    /* empty */
  }
}
