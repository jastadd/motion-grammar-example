package de.tudresden.inf.st.mg.common;

import java.nio.file.Path;

public class MotionGrammarConfig {

  private MotionGrammarConfig() {
    // hide the constructor
  }

  public static Path astDiagramDir;
  public static final String DEFAULT_COLOR = "cccccc";
  public static final String ADD_COLOR = "47ba4c";
  public static final String REMOVE_COLOR = "de4a2c";


}
