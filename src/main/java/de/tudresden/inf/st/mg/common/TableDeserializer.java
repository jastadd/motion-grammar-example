package de.tudresden.inf.st.mg.common;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ValueNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.tudresden.inf.st.mg.jastadd.model.*;

import java.io.IOException;

public class TableDeserializer extends JsonDeserializer<Table> {

    // idea taken from https://gist.github.com/sverhagen/c199d924245a078c287b46546e1ac26c
    // TODO this looks strange, since we should already have an ObjectMapper
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper(new YAMLFactory());

    @Override
    public Table deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        JsonNode node = p.getCodec().readTree(p);

        Table result = new Table();
        result.setRobot(new Robot());

        if (node.isObject() && node.get("objects").isArray()) {
            for (TreeNode child : node.get("objects")) {
                TreeNode typeNode = child.get("type");
                String type = "UNKNOWN";
                if (typeNode != null && typeNode.isValueNode()) {
                    type = ((ValueNode) typeNode).asText();
                }
                switch (type) {
                    case "BIN":
                        Bin bin = OBJECT_MAPPER.convertValue(child, Bin.class);
                        bin.setPose(new Pose(0,0,0,0,0,0,1)); //TODO use real pose
                        result.addPhysicalObject(bin);
                        break;
                    case "BOX":
                        MovableObject movableObject = OBJECT_MAPPER.convertValue(child, MovableObject.class);
                        movableObject.setPose(new Pose(0,0,0,0,0,0,1)); //TODO use real pose
                        result.addPhysicalObject(movableObject);
                        break;
                    case "ARM":
                    case "UNKNOWN":
                        // ignore the arm
                        break;
                    default:
                        System.out.println("ignoring scene content of type " + type);
                }

            }
        } else {
            throw new IOException("YAML node is not an object with an 'objects' array.");
        }

        return result;
    }
}
