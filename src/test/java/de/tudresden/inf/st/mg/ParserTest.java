package de.tudresden.inf.st.mg;

import de.tudresden.inf.st.mg.common.MotionGrammarParser;
import de.tudresden.inf.st.mg.jastadd.model.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class ParserTest {

  public static final Path LOAD_AST_DIAGRAM_DIR = Path.of("src", "gen", "resources", "diagrams", "parsing", "load");
  public static final Path TIDY_AST_DIAGRAM_DIR = Path.of("src", "gen", "resources", "diagrams", "parsing", "tidy");

  @BeforeAll
  static void prepareOutputPath() throws IOException {
    try {
      Files.walk(LOAD_AST_DIAGRAM_DIR).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
    } catch (IOException e) {
      // do nothing
    }
    try {
      Files.walk(TIDY_AST_DIAGRAM_DIR).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
    } catch (IOException e) {
      // do nothing
    }
    Files.createDirectories(LOAD_AST_DIAGRAM_DIR);
    Files.createDirectories(TIDY_AST_DIAGRAM_DIR);

    World.enableContextPrinting(true);
  }

  @Test
  void runLodUnloadParser() throws MotionGrammarParser.ParseException {

    MotionGrammarParser.setDebugDiagramDir(LOAD_AST_DIAGRAM_DIR);

    // for some reason, the best random seed value here is 1 and not 0???
    Container containerWorld = Container.initialWorld(new Random(1));

    LoadWorldParser parser = new LoadWorldParser(containerWorld);

    var result = parser.parse();

    assertThat(result).isNotNull().isInstanceOf(T1.class);
  }

  @Test
  void runTidyParser() throws MotionGrammarParser.ParseException {

    MotionGrammarParser.setDebugDiagramDir(TIDY_AST_DIAGRAM_DIR);

    // for some reason, the best random seed value here is 1 and not 0???
    RobotWorld world = RobotWorld.initialWorld(new Random(1));
    world.printContext("initial");

    // create a parser using the world
    RobotParser parser = new RobotParser(world);

    world.addSelection(new Selection().setObject("boxRed").setTimeStamp(java.time.Instant.now()))
        .addSelection(new Selection().setObject("boxGreen").setTimeStamp(java.time.Instant.now()))
        .addSelection(new Selection().setObject("binGreen").setTimeStamp(java.time.Instant.now()))
        .addSelection(new Selection().setObject("binRed").setTimeStamp(java.time.Instant.now()))
        .addSelection(new Selection().setObject("boxRed").setTimeStamp(java.time.Instant.now()))
        .addSelection(new Selection().setObject("boxRed").setTimeStamp(java.time.Instant.now()));

    // parse (synchronously, long-running)
    var result = parser.parse();

    assertThat(result).isNotNull().isInstanceOf(Tidy.class);
  }

}
