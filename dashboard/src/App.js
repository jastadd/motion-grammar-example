import './App.css';
import React, { useEffect, useRef } from 'react';
import './index.css';
import 'react-vertical-timeline-component/style.min.css';
import { useSelector, useDispatch } from 'react-redux'
import { setAst, setContext } from './mgSlice'

function App() {

  const ws = useRef(null);
  const dispatch = useDispatch();
  const ast = useSelector(state => state.mg.ast);
  const context = useSelector(state => state.mg.context);

  useEffect(() => {
    ws.current = new WebSocket("ws://localhost:7070/ast-events");
    ws.current.onopen = () => console.log("ws opened");
    ws.current.onclose = () => console.log("ws closed");
    ws.current.onmessage = (event) => {
      const json = JSON.parse(event.data);
      try {
        if (json.type === "ast") {
          dispatch(setAst(json));
        } else if (json.type === "context") {
          dispatch(setContext(json));
        }
      } catch (err) {
        console.log(err);
      }
    };

    const wsCurrent = ws.current;

    return () => {
      wsCurrent.close();
    };
  }, [dispatch]);

  useEffect(() => {
    if (ast.diagram == null) {
      console.log("setting ast from WS 1");
      fetch('http://localhost:7070/ast/')
        .then(res => res.json())
        .then((ast) => {
            console.log("setting ast from WS");
            console.log(ast);
          dispatch(setAst(ast))
        })
        .catch(console.log)
    }
    if (context.diagram == null) {
      console.log("setting ctx from WS");
      fetch('http://localhost:7070/context/')
          .then(res => res.json())
          .then((context) => {
            dispatch(setContext(context))
          })
          .catch(console.log)
    } else {
      console.log(context)
    }
  }, [dispatch, ast, context])


  return (
    <div className="App">
      <header className="App-header">
        <ControlPanel
          ws={ws}
        />
      </header>
      <main>
        <CurrentContextState
          diagram={context ? context.diagram : ''}
          label=''
        />
          <CurrentASTState
              diagram={ast ? ast.diagram : ''}
              label={ast ? ast.parseRule : ''}
          />
      </main>
    </div>
  );
}

function CurrentASTState(props) {
  return (
    <div className="AstContainer">
      <img className="CurrentAst" src={`data:image/svg+xml;utf8,${encodeURIComponent(props.diagram)}`} alt={props.label} />
    </div>
  )
}

function CurrentContextState(props) {
    return (
        <div className="ContextContainer">
            <img className="CurrentContext" src={`data:image/svg+xml;utf8,${encodeURIComponent(props.diagram)}`} alt={props.label} />
        </div>
    )
}

function ControlPanel(props) {
  return (
    <div className='ControlPanel'>
      <button onClick={() => { props.ws.current.send("cell1 parse") }}>start parsing in cell 1</button>
      <button onClick={() => { props.ws.current.send("cell1 reset") }}>reset selections</button>
    </div>
  )
}

export default App;
