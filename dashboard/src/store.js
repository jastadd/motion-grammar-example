import { configureStore } from '@reduxjs/toolkit'
import mgReducer from './mgSlice'

export default configureStore({
  reducer: {
    mg: mgReducer
  }
})