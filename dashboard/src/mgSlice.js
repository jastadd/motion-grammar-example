import {createSlice} from '@reduxjs/toolkit'

export const mgSlice = createSlice({
    name: 'mg',
    initialState: {
        ast: {},
        context: {},
    },
    reducers: {
        setAst: (state, action) => {
            state.ast = action.payload;
            console.log("set AST");
        },
        setContext: (state, action) => {
            state.context = action.payload;
            console.log("set context");
        }
    }
})

export const {setAst, setContext} = mgSlice.actions

export default mgSlice.reducer