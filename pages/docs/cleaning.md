# The Robot Sorting Grammar

This is the most recent version of the motion grammar execution.
Compared to the version published as an ACSOS demo, there are some changes here:

- If no token can be parsed, the grammar waits for a parsable token.
- An input `Selection` programs a decision tree that determines where an object should be put.

## Grammar

![](diagrams/grammar/cleaning.png)

## Video

![type:video](https://www.youtube.com/embed/hsK4gnoM8hg)
