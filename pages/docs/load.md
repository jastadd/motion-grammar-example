# The Load/Unload Grammar

The motion grammar describes the Load/Unload scenario from Dantam[^1]. The point of this very simple grammar was *not*
to show how motion grammars work, but rather the expressiveness of a CFG (compared to a regular grammar), but it is very
simple, so it serves as a starting point.

![](diagrams/grammar/loading.png)

The grammar comprises three parts. `MotionGrammar` contains generic nonterminals present in every motion
grammar, `LoadUnload` has the actual nonterminals of the grammar, and `World` describes the context within which the
grammar is parsed.

## Execution (Parsing) and Implementation Details

Currently, the parser reads a sequence of `Load`, `Unload`, and `Full` tokens and loads a `Container` with up to ten
elements.

There are no attributes used so far, but at each point where a semantic action takes place, the AST is printed to a file
in `src/gen/resources/diagrams/parsing`, demonstrating the "available" part of the AST, which could be used by
attributes. This trace is shown [here](/load-parsing).

Detailed notes:

- Tokens can have semantic actions. This might be a difference to the original motion grammar concept, but could easily
  be simulated by adding another nonterminal enclosing each token.
- The `Load` token currently can always be parsed. This is problematic, since it requires a fixed order in the parser
  rule for `T`. A workaround would be to introduce a new token `NotFull` before the load token.
- The parser `Worldparser.java` uses three kinds of parse methods:
    - `parse` distinguishes an abstract rule (for `T`, but since it is the entrypoint, it has this generic name). This
      only works like this if the abstract rule does *not* have any elements, which can be achieved by a simple
      transformation, but maybe breaks the attribute part of the grammar. This method needs a lookahead, which is done
      by a peek method. The peek method actually parses the next token and keeps it in a cache, which is read by the
      token parse method later.
    - `parseT1` and `parseT2` do not need lookahead.
    - `parseLoad`, `parseUnload`, and `parseFull` parse tokens.
- There are no thoughts on parsing lists and optional children yet.

## Bibliogaphy

[^1]: Dantam, N., Stilman, M., 2013. The Motion Grammar: Analysis of a Linguistic Method for Robot Control. IEEE Trans.
Robot. 29, 704–718. https://doi.org/10.1109/TRO.2013.2239553

