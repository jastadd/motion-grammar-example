#!/bin/bash
rm -f table.csv
for filename in *.svg
do
  fwe="${filename%.svg}"
  split=`echo $fwe | tr "-" " "`
  echo "$split $filename" >> table.csv
done
sort -t ' ' -k 3,3 table.csv > table-sorted.csv

cat << EOF > ../cleaning-parsing.md
# Parsing the Robot Sorting World (Current Version)

| Action                     | Context Update  | Timestamp    | Parse Tree                                                                             |
|----------------------------|-----------------|--------------|----------------------------------------------------------------------------------------|
EOF

awk '{gsub("\\.", ":", $3);gsub("\\(\\)", "", $4); print "| " (($1=="AST")?$4:"-") " | " (($1=="Context")?$4:"-") " | " substr($3,12) " | [![" (($1=="AST")?$5:$6) "](cleaning/" (($1=="AST")?$5:$6) ")](cleaning/" (($1=="AST")?$5:$6) ") |"}' table-sorted.csv >> ../cleaning-parsing.md
rm table.csv
rm table-sorted.csv
