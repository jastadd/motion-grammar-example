# Motion Grammar Parser for JastAdd Examples

A testbed for a potential *motion grammar*[^1] implementation using JastAdd.
For the source code, check out the [git repository](https://git-st.inf.tu-dresden.de/jastadd/motion-grammar-example).

## Introduction
This is an implementation of motion grammars from Dantam[^1] using Reference Attribute Grammars. In this implementation,
both the context model and the motion grammar are described using attribute grammars.

## Motion Grammars in JastAdd

A JastAdd motion grammar comprises three parts. `MotionGrammar` contains generic nonterminals present in every motion
grammar, a second, use-case-specific grammar has the actual nonterminals of the grammar as well as additonal information
to construct a parser, and finally a third *context grammar* that describes the context within which the motion grammar 
is parsed.

## Use cases

- Very simple [Load-Unload grammar](load.md) taken from the original paper [^1] to show a minimal proof-of-concept.
- A [robotic use case](acsos.md) sorting dynamically appearing objects into boxes by colour ([ACSOS demo]()https://2022.acsos.org/details/acsos-2022-posters-and-demos/2/Specifying-Reactive-Robotic-Applications-With-Reference-Attribute-Motion-Grammars).
  ![type:video](https://www.youtube.com/embed/KBuFVsKjA1Q)
- A similar robotic use case, but with another input "teaching" where to put the objects and different time 
## Bibliogaphy

[^1]: Dantam, N., Stilman, M., 2013. The Motion Grammar: Analysis of a Linguistic Method for Robot Control. IEEE Trans. Robot. 29, 704–718. https://doi.org/10.1109/TRO.2013.2239553

