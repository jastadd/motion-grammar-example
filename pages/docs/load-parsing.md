# Parsing the Load/Unload World
| Action | Model                                                                                        |
|--------|----------------------------------------------------------------------------------------------|
|        | ![](loading/Context-Container-2023.01.05.20.07.04.557-Container.setElementCount()-AFTER.svg) |
| Load   | ![](loading/AST-001-2023.01.05.20.07.07.622-parseLoad.svg)                                   |
|        | ![](loading/Context-Container-2023.01.05.20.07.07.399-Container.setElementCount()-AFTER.svg) |
| Load   | ![](loading/AST-002-2023.01.05.20.07.08.694-parseLoad.svg)                                   |
|        | ![](loading/Context-Container-2023.01.05.20.07.08.463-Container.setElementCount()-AFTER.svg) |
| Load   | ![](loading/AST-003-2023.01.05.20.07.09.233-parseLoad.svg)                                   |
|        | ![](loading/Context-Container-2023.01.05.20.07.09.117-Container.setElementCount()-AFTER.svg) |
|        | ![](loading/AST-004-2023.01.05.20.07.09.551-parseFull.svg)                                   |
|        | ![](loading/AST-005-2023.01.05.20.07.10.073-parseT2.svg)                                     |
|        | ![](loading/AST-006-2023.01.05.20.07.10.402-parseT.svg)                                      |
| Unload | ![](loading/AST-007-2023.01.05.20.07.10.929-parseUnload.svg)                                 |
|        | ![](loading/Context-Container-2023.01.05.20.07.10.793-Container.setElementCount()-AFTER.svg) |
|        | ![](loading/AST-008-2023.01.05.20.07.11.288-parseT1.svg)                                     |
|        | ![](loading/AST-009-2023.01.05.20.07.11.565-parseT.svg)                                      |
| Unload | ![](loading/AST-010-2023.01.05.20.07.11.959-parseUnload.svg)                                 |
|        | ![](loading/Context-Container-2023.01.05.20.07.11.898-Container.setElementCount()-AFTER.svg) |
|        | ![](loading/AST-011-2023.01.05.20.07.12.257-parseT1.svg)                                     |
|        | ![](loading/AST-012-2023.01.05.20.07.12.464-parseT.svg)                                      |
| Unload | ![](loading/AST-013-2023.01.05.20.07.12.710-parseUnload.svg)                                 |
|        | ![](loading/Context-Container-2023.01.05.20.07.12.655-Container.setElementCount()-AFTER.svg) |
|        | ![](loading/AST-014-2023.01.05.20.07.12.983-parseT1.svg)                                     |
|        | ![](loading/AST-015-2023.01.05.20.07.13.139-parseT.svg)                                      |
|        | ![](loading/AST-016-2023.01.05.20.07.13.258-complete.svg)                                    |
