# Parsing the Robot Sorting World (ACSOS Demo Paper Version)

| Action                     | Context Update  | Timestamp    | Parse Tree                                                                             |
|----------------------------|-----------------|--------------|----------------------------------------------------------------------------------------|
|                            |                 | 17.43.35.056 | ![](acsos/Context-RobotWorld-2022.07.18.17.43.35.056-RobotWorld.setTable()-DONE.svg) |
|                            |                 | 17.43.35.270 | ![](acsos/AST-000-2022.07.18.17.43.35.270-initial.svg)                               |
| EmptyTable                 |                 | 17.43.39.588 | ![](acsos/AST-001-2022.07.18.17.43.39.588-parseEmptyTable.svg)                       |
| Wait                       |                 | 17.43.47.871 | ![](acsos/AST-002-2022.07.18.17.43.47.871-parseWait.svg)                             |
| WaitForFullTable           |                 | 17.43.48.060 | ![](acsos/AST-003-2022.07.18.17.43.48.060-parseWaitForFullTable.svg)                 |
| EmptyTable                 |                 | 17.43.48.272 | ![](acsos/AST-004-2022.07.18.17.43.48.272-parseEmptyTable.svg)                       |
| Wait                       |                 | 17.43.56.518 | ![](acsos/AST-005-2022.07.18.17.43.56.518-parseWait.svg)                             |
| WaitForFullTable           |                 | 17.43.56.718 | ![](acsos/AST-006-2022.07.18.17.43.56.718-parseWaitForFullTable.svg)                 |
| EmptyTable                 |                 | 17.43.56.889 | ![](acsos/AST-007-2022.07.18.17.43.56.889-parseEmptyTable.svg)                       |
|                            | object added    | 17.43.59.407 | ![](acsos/Context-RobotWorld-2022.07.18.17.43.59.407-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.44.05.105 | ![](acsos/AST-008-2022.07.18.17.44.05.105-parseWait.svg)                             |
| WaitForFullTable           |                 | 17.44.05.370 | ![](acsos/AST-009-2022.07.18.17.44.05.370-parseWaitForFullTable.svg)                 |
| ObjectAtWrongPlace         |                 | 17.44.05.769 | ![](acsos/AST-010-2022.07.18.17.44.05.769-parseObjectAtWrongPlace.svg)               |
| RobotIsReadyToPickToken    |                 | 17.44.06.152 | ![](acsos/AST-011-2022.07.18.17.44.06.152-parseRobotIsReadyToPickToken.svg)          |
| RobotIsReallyReadyToPick   |                 | 17.44.06.526 | ![](acsos/AST-012-2022.07.18.17.44.06.526-parseRobotIsReallyReadyToPick.svg)         |
| RobotIsReadyToPick         |                 | 17.44.06.859 | ![](acsos/AST-013-2022.07.18.17.44.06.859-parseRobotIsReadyToPick.svg)               |
| PickUpObject               |                 | 17.44.07.284 | ![](acsos/AST-014-2022.07.18.17.44.07.284-parsePickUpObject.svg)                     |
|                            | object attached | 17.44.07.337 | ![](acsos/Context-RobotWorld-2022.07.18.17.44.07.337-RobotWorld.setTable()-DONE.svg) |
| RobotIsNotReadyToDropToken |                 | 17.44.07.807 | ![](acsos/AST-015-2022.07.18.17.44.07.807-parseRobotIsNotReadyToDropToken.svg)       |
| Wait                       |                 | 17.44.16.137 | ![](acsos/AST-016-2022.07.18.17.44.16.137-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.44.16.336 | ![](acsos/AST-017-2022.07.18.17.44.16.336-parseRobotIsNotReadyToDropToken.svg)       |
|                            | object added    | 17.44.22.245 | ![](acsos/Context-RobotWorld-2022.07.18.17.44.22.245-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.44.24.593 | ![](acsos/AST-018-2022.07.18.17.44.24.593-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.44.24.996 | ![](acsos/AST-019-2022.07.18.17.44.24.996-parseRobotIsNotReadyToDropToken.svg)       |
|                            | robot idle      | 17.44.25.162 | ![](acsos/Context-RobotWorld-2022.07.18.17.44.25.162-RobotWorld.setTable()-DONE.svg) |
|                            | object added    | 17.44.30.153 | ![](acsos/Context-RobotWorld-2022.07.18.17.44.30.153-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.44.33.736 | ![](acsos/AST-020-2022.07.18.17.44.33.736-parseWait.svg)                             |
| RobotIsReadyToDropToken    |                 | 17.44.34.344 | ![](acsos/AST-021-2022.07.18.17.44.34.344-parseRobotIsReadyToDropToken.svg)          |
| RobotIsReallyReadyToDrop   |                 | 17.44.34.795 | ![](acsos/AST-022-2022.07.18.17.44.34.795-parseRobotIsReallyReadyToDrop.svg)         |
| RobotIsReadyToDrop         |                 | 17.44.35.148 | ![](acsos/AST-023-2022.07.18.17.44.35.148-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.44.35.616 | ![](acsos/AST-024-2022.07.18.17.44.35.616-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.44.36.052 | ![](acsos/AST-025-2022.07.18.17.44.36.052-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.44.36.493 | ![](acsos/AST-026-2022.07.18.17.44.36.493-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.44.36.798 | ![](acsos/AST-027-2022.07.18.17.44.36.798-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.44.37.173 | ![](acsos/AST-028-2022.07.18.17.44.37.173-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.44.37.512 | ![](acsos/AST-029-2022.07.18.17.44.37.512-parseRobotIsReadyToDrop.svg)               |
| RightPlace                 |                 | 17.44.37.872 | ![](acsos/AST-030-2022.07.18.17.44.37.872-parseRightPlace.svg)                       |
| DropObjectAtRightPlace     |                 | 17.44.38.308 | ![](acsos/AST-031-2022.07.18.17.44.38.308-parseDropObjectAtRightPlace.svg)           |
| MoveObjectToCorrectPlace   |                 | 17.44.38.799 | ![](acsos/AST-032-2022.07.18.17.44.38.799-parseMoveObjectToCorrectPlace.svg)         |
| ObjectAtWrongPlace         |                 | 17.44.39.211 | ![](acsos/AST-033-2022.07.18.17.44.39.211-parseObjectAtWrongPlace.svg)               |
| RobotIsNotReadyToPickToken |                 | 17.44.39.524 | ![](acsos/AST-034-2022.07.18.17.44.39.524-parseRobotIsNotReadyToPickToken.svg)       |
| Wait                       |                 | 17.44.47.970 | ![](acsos/AST-035-2022.07.18.17.44.47.970-parseWait.svg)                             |
| RobotIsNotReadyToPickToken |                 | 17.44.48.142 | ![](acsos/AST-036-2022.07.18.17.44.48.142-parseRobotIsNotReadyToPickToken.svg)       |
|                            | object gone     | 17.44.54.865 | ![](acsos/Context-RobotWorld-2022.07.18.17.44.54.865-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.44.56.356 | ![](acsos/AST-037-2022.07.18.17.44.56.356-parseWait.svg)                             |
| RobotIsReadyToPickToken    |                 | 17.44.56.572 | ![](acsos/AST-038-2022.07.18.17.44.56.572-parseRobotIsReadyToPickToken.svg)          |
| RobotIsReallyReadyToPick   |                 | 17.44.56.825 | ![](acsos/AST-039-2022.07.18.17.44.56.825-parseRobotIsReallyReadyToPick.svg)         |
| RobotIsReadyToPick         |                 | 17.44.57.094 | ![](acsos/AST-040-2022.07.18.17.44.57.094-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.44.57.328 | ![](acsos/AST-041-2022.07.18.17.44.57.328-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.44.57.607 | ![](acsos/AST-042-2022.07.18.17.44.57.607-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.44.57.896 | ![](acsos/AST-043-2022.07.18.17.44.57.896-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.44.58.169 | ![](acsos/AST-044-2022.07.18.17.44.58.169-parseRobotIsReadyToPick.svg)               |
| PickUpObject               |                 | 17.44.58.456 | ![](acsos/AST-045-2022.07.18.17.44.58.456-parsePickUpObject.svg)                     |
|                            | object attached | 17.44.58.520 | ![](acsos/Context-RobotWorld-2022.07.18.17.44.58.520-RobotWorld.setTable()-DONE.svg) |
| RobotIsNotReadyToDropToken |                 | 17.44.58.783 | ![](acsos/AST-046-2022.07.18.17.44.58.783-parseRobotIsNotReadyToDropToken.svg)       |
| Wait                       |                 | 17.45.07.121 | ![](acsos/AST-047-2022.07.18.17.45.07.121-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.45.07.305 | ![](acsos/AST-048-2022.07.18.17.45.07.305-parseRobotIsNotReadyToDropToken.svg)       |
|                            | object added    | 17.45.13.036 | ![](acsos/Context-RobotWorld-2022.07.18.17.45.13.036-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.45.15.564 | ![](acsos/AST-049-2022.07.18.17.45.15.564-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.45.15.800 | ![](acsos/AST-050-2022.07.18.17.45.15.800-parseRobotIsNotReadyToDropToken.svg)       |
|                            | robot is idle   | 17.45.19.256 | ![](acsos/Context-RobotWorld-2022.07.18.17.45.19.256-RobotWorld.setTable()-DONE.svg) |
|                            | object added    | 17.45.21.852 | ![](acsos/Context-RobotWorld-2022.07.18.17.45.21.852-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.45.24.125 | ![](acsos/AST-051-2022.07.18.17.45.24.125-parseWait.svg)                             |
| RobotIsReadyToDropToken    |                 | 17.45.24.328 | ![](acsos/AST-052-2022.07.18.17.45.24.328-parseRobotIsReadyToDropToken.svg)          |
| RobotIsReallyReadyToDrop   |                 | 17.45.24.535 | ![](acsos/AST-053-2022.07.18.17.45.24.535-parseRobotIsReallyReadyToDrop.svg)         |
| RobotIsReadyToDrop         |                 | 17.45.24.828 | ![](acsos/AST-054-2022.07.18.17.45.24.828-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.45.25.133 | ![](acsos/AST-055-2022.07.18.17.45.25.133-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.45.25.385 | ![](acsos/AST-056-2022.07.18.17.45.25.385-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.45.25.689 | ![](acsos/AST-057-2022.07.18.17.45.25.689-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.45.25.998 | ![](acsos/AST-058-2022.07.18.17.45.25.998-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.45.26.299 | ![](acsos/AST-059-2022.07.18.17.45.26.299-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.45.26.574 | ![](acsos/AST-060-2022.07.18.17.45.26.574-parseRobotIsReadyToDrop.svg)               |
| RightPlace                 |                 | 17.45.26.864 | ![](acsos/AST-061-2022.07.18.17.45.26.864-parseRightPlace.svg)                       |
| DropObjectAtRightPlace     |                 | 17.45.27.156 | ![](acsos/AST-062-2022.07.18.17.45.27.156-parseDropObjectAtRightPlace.svg)           |
| MoveObjectToCorrectPlace   |                 | 17.45.27.617 | ![](acsos/AST-063-2022.07.18.17.45.27.617-parseMoveObjectToCorrectPlace.svg)         |
| ObjectAtWrongPlace         |                 | 17.45.27.991 | ![](acsos/AST-064-2022.07.18.17.45.27.991-parseObjectAtWrongPlace.svg)               |
| RobotIsNotReadyToPickToken |                 | 17.45.28.330 | ![](acsos/AST-065-2022.07.18.17.45.28.330-parseRobotIsNotReadyToPickToken.svg)       |
| Wait                       |                 | 17.45.36.597 | ![](acsos/AST-066-2022.07.18.17.45.36.597-parseWait.svg)                             |
| RobotIsNotReadyToPickToken |                 | 17.45.37.022 | ![](acsos/AST-067-2022.07.18.17.45.37.022-parseRobotIsNotReadyToPickToken.svg)       |
|                            | object added    | 17.45.43.100 | ![](acsos/Context-RobotWorld-2022.07.18.17.45.43.100-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.45.45.385 | ![](acsos/AST-068-2022.07.18.17.45.45.385-parseWait.svg)                             |
| RobotIsNotReadyToPickToken |                 | 17.45.45.808 | ![](acsos/AST-069-2022.07.18.17.45.45.808-parseRobotIsNotReadyToPickToken.svg)       |
|                            | object gone     | 17.45.47.121 | ![](acsos/Context-RobotWorld-2022.07.18.17.45.47.121-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.45.54.253 | ![](acsos/AST-070-2022.07.18.17.45.54.253-parseWait.svg)                             |
| RobotIsReadyToPickToken    |                 | 17.45.54.524 | ![](acsos/AST-071-2022.07.18.17.45.54.524-parseRobotIsReadyToPickToken.svg)          |
| RobotIsReallyReadyToPick   |                 | 17.45.54.815 | ![](acsos/AST-072-2022.07.18.17.45.54.815-parseRobotIsReallyReadyToPick.svg)         |
| RobotIsReadyToPick         |                 | 17.45.55.141 | ![](acsos/AST-073-2022.07.18.17.45.55.141-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.45.55.508 | ![](acsos/AST-074-2022.07.18.17.45.55.508-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.45.55.795 | ![](acsos/AST-075-2022.07.18.17.45.55.795-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.45.56.159 | ![](acsos/AST-076-2022.07.18.17.45.56.159-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.45.56.569 | ![](acsos/AST-077-2022.07.18.17.45.56.569-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.45.56.870 | ![](acsos/AST-078-2022.07.18.17.45.56.870-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.45.57.191 | ![](acsos/AST-079-2022.07.18.17.45.57.191-parseRobotIsReadyToPick.svg)               |
| PickUpObject               |                 | 17.45.57.480 | ![](acsos/AST-080-2022.07.18.17.45.57.480-parsePickUpObject.svg)                     |
|                            | object attached | 17.45.57.532 | ![](acsos/Context-RobotWorld-2022.07.18.17.45.57.532-RobotWorld.setTable()-DONE.svg) |
| RobotIsNotReadyToDropToken |                 | 17.45.57.813 | ![](acsos/AST-081-2022.07.18.17.45.57.813-parseRobotIsNotReadyToDropToken.svg)       |
| Wait                       |                 | 17.46.06.099 | ![](acsos/AST-082-2022.07.18.17.46.06.099-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.46.06.350 | ![](acsos/AST-083-2022.07.18.17.46.06.350-parseRobotIsNotReadyToDropToken.svg)       |
| Wait                       |                 | 17.46.14.753 | ![](acsos/AST-084-2022.07.18.17.46.14.753-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.46.15.104 | ![](acsos/AST-085-2022.07.18.17.46.15.104-parseRobotIsNotReadyToDropToken.svg)       |
|                            | robot is idle   | 17.46.20.101 | ![](acsos/Context-RobotWorld-2022.07.18.17.46.20.101-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.46.23.512 | ![](acsos/AST-086-2022.07.18.17.46.23.512-parseWait.svg)                             |
| RobotIsReadyToDropToken    |                 | 17.46.23.794 | ![](acsos/AST-087-2022.07.18.17.46.23.794-parseRobotIsReadyToDropToken.svg)          |
| RobotIsReallyReadyToDrop   |                 | 17.46.24.101 | ![](acsos/AST-088-2022.07.18.17.46.24.101-parseRobotIsReallyReadyToDrop.svg)         |
| RobotIsReadyToDrop         |                 | 17.46.24.486 | ![](acsos/AST-089-2022.07.18.17.46.24.486-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.46.24.794 | ![](acsos/AST-090-2022.07.18.17.46.24.794-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.46.25.117 | ![](acsos/AST-091-2022.07.18.17.46.25.117-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.46.25.461 | ![](acsos/AST-092-2022.07.18.17.46.25.461-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.46.25.965 | ![](acsos/AST-093-2022.07.18.17.46.25.965-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.46.26.445 | ![](acsos/AST-094-2022.07.18.17.46.26.445-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.46.26.875 | ![](acsos/AST-095-2022.07.18.17.46.26.875-parseRobotIsReadyToDrop.svg)               |
| RightPlace                 |                 | 17.46.27.270 | ![](acsos/AST-096-2022.07.18.17.46.27.270-parseRightPlace.svg)                       |
| DropObjectAtRightPlace     |                 | 17.46.27.571 | ![](acsos/AST-097-2022.07.18.17.46.27.571-parseDropObjectAtRightPlace.svg)           |
| MoveObjectToCorrectPlace   |                 | 17.46.28.040 | ![](acsos/AST-098-2022.07.18.17.46.28.040-parseMoveObjectToCorrectPlace.svg)         |
| ObjectAtWrongPlace         |                 | 17.46.28.378 | ![](acsos/AST-099-2022.07.18.17.46.28.378-parseObjectAtWrongPlace.svg)               |
| RobotIsNotReadyToPickToken |                 | 17.46.28.783 | ![](acsos/AST-100-2022.07.18.17.46.28.783-parseRobotIsNotReadyToPickToken.svg)       |
| Wait                       |                 | 17.46.37.214 | ![](acsos/AST-101-2022.07.18.17.46.37.214-parseWait.svg)                             |
| RobotIsNotReadyToPickToken |                 | 17.46.37.839 | ![](acsos/AST-102-2022.07.18.17.46.37.839-parseRobotIsNotReadyToPickToken.svg)       |
|                            | object added    | 17.46.40.928 | ![](acsos/Context-RobotWorld-2022.07.18.17.46.40.928-RobotWorld.setTable()-DONE.svg) |
|                            | object gone     | 17.46.42.453 | ![](acsos/Context-RobotWorld-2022.07.18.17.46.42.453-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.46.46.415 | ![](acsos/AST-103-2022.07.18.17.46.46.415-parseWait.svg)                             |
| RobotIsReadyToPickToken    |                 | 17.46.46.772 | ![](acsos/AST-104-2022.07.18.17.46.46.772-parseRobotIsReadyToPickToken.svg)          |
| RobotIsReallyReadyToPick   |                 | 17.46.47.259 | ![](acsos/AST-105-2022.07.18.17.46.47.259-parseRobotIsReallyReadyToPick.svg)         |
| RobotIsReadyToPick         |                 | 17.46.47.773 | ![](acsos/AST-106-2022.07.18.17.46.47.773-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.46.48.234 | ![](acsos/AST-107-2022.07.18.17.46.48.234-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.46.48.689 | ![](acsos/AST-108-2022.07.18.17.46.48.689-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.46.49.022 | ![](acsos/AST-109-2022.07.18.17.46.49.022-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.46.49.401 | ![](acsos/AST-110-2022.07.18.17.46.49.401-parseRobotIsReadyToPick.svg)               |
| PickUpObject               |                 | 17.46.49.769 | ![](acsos/AST-111-2022.07.18.17.46.49.769-parsePickUpObject.svg)                     |
|                            | object attached | 17.46.49.833 | ![](acsos/Context-RobotWorld-2022.07.18.17.46.49.833-RobotWorld.setTable()-DONE.svg) |
| RobotIsNotReadyToDropToken |                 | 17.46.50.301 | ![](acsos/AST-112-2022.07.18.17.46.50.301-parseRobotIsNotReadyToDropToken.svg)       |
| Wait                       |                 | 17.46.58.748 | ![](acsos/AST-113-2022.07.18.17.46.58.748-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.46.59.083 | ![](acsos/AST-114-2022.07.18.17.46.59.083-parseRobotIsNotReadyToDropToken.svg)       |
| Wait                       |                 | 17.47.07.498 | ![](acsos/AST-115-2022.07.18.17.47.07.498-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.47.07.880 | ![](acsos/AST-116-2022.07.18.17.47.07.880-parseRobotIsNotReadyToDropToken.svg)       |
|                            | robot idle      | 17.47.08.325 | ![](acsos/Context-RobotWorld-2022.07.18.17.47.08.325-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.47.16.397 | ![](acsos/AST-117-2022.07.18.17.47.16.397-parseWait.svg)                             |
| RobotIsReadyToDropToken    |                 | 17.47.16.801 | ![](acsos/AST-118-2022.07.18.17.47.16.801-parseRobotIsReadyToDropToken.svg)          |
| RobotIsReallyReadyToDrop   |                 | 17.47.17.359 | ![](acsos/AST-119-2022.07.18.17.47.17.359-parseRobotIsReallyReadyToDrop.svg)         |
| RobotIsReadyToDrop         |                 | 17.47.17.845 | ![](acsos/AST-120-2022.07.18.17.47.17.845-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.47.18.346 | ![](acsos/AST-121-2022.07.18.17.47.18.346-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.47.18.751 | ![](acsos/AST-122-2022.07.18.17.47.18.751-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.47.19.190 | ![](acsos/AST-123-2022.07.18.17.47.19.190-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.47.19.584 | ![](acsos/AST-124-2022.07.18.17.47.19.584-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.47.20.046 | ![](acsos/AST-125-2022.07.18.17.47.20.046-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.47.20.492 | ![](acsos/AST-126-2022.07.18.17.47.20.492-parseRobotIsReadyToDrop.svg)               |
| RightPlace                 |                 | 17.47.20.938 | ![](acsos/AST-127-2022.07.18.17.47.20.938-parseRightPlace.svg)                       |
| DropObjectAtRightPlace     |                 | 17.47.21.359 | ![](acsos/AST-128-2022.07.18.17.47.21.359-parseDropObjectAtRightPlace.svg)           |
| MoveObjectToCorrectPlace   |                 | 17.47.21.787 | ![](acsos/AST-129-2022.07.18.17.47.21.787-parseMoveObjectToCorrectPlace.svg)         |
| ObjectAtWrongPlace         |                 | 17.47.22.194 | ![](acsos/AST-130-2022.07.18.17.47.22.194-parseObjectAtWrongPlace.svg)               |
| RobotIsNotReadyToPickToken |                 | 17.47.22.574 | ![](acsos/AST-131-2022.07.18.17.47.22.574-parseRobotIsNotReadyToPickToken.svg)       |
| Wait                       |                 | 17.47.31.018 | ![](acsos/AST-132-2022.07.18.17.47.31.018-parseWait.svg)                             |
|                            | object gone     | 17.47.31.345 | ![](acsos/Context-RobotWorld-2022.07.18.17.47.31.345-RobotWorld.setTable()-DONE.svg) |
| RobotIsReadyToPickToken    |                 | 17.47.31.661 | ![](acsos/AST-133-2022.07.18.17.47.31.661-parseRobotIsReadyToPickToken.svg)          |
| RobotIsReallyReadyToPick   |                 | 17.47.32.242 | ![](acsos/AST-134-2022.07.18.17.47.32.242-parseRobotIsReallyReadyToPick.svg)         |
| RobotIsReadyToPick         |                 | 17.47.32.687 | ![](acsos/AST-135-2022.07.18.17.47.32.687-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.47.33.236 | ![](acsos/AST-136-2022.07.18.17.47.33.236-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.47.33.791 | ![](acsos/AST-137-2022.07.18.17.47.33.791-parseRobotIsReadyToPick.svg)               |
| PickUpObject               |                 | 17.47.34.349 | ![](acsos/AST-138-2022.07.18.17.47.34.349-parsePickUpObject.svg)                     |
|                            | object attached | 17.47.34.407 | ![](acsos/Context-RobotWorld-2022.07.18.17.47.34.407-RobotWorld.setTable()-DONE.svg) |
| RobotIsNotReadyToDropToken |                 | 17.47.35.136 | ![](acsos/AST-139-2022.07.18.17.47.35.136-parseRobotIsNotReadyToDropToken.svg)       |
| Wait                       |                 | 17.47.44.054 | ![](acsos/AST-140-2022.07.18.17.47.44.054-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.47.44.477 | ![](acsos/AST-141-2022.07.18.17.47.44.477-parseRobotIsNotReadyToDropToken.svg)       |
|                            | robot idle      | 17.47.52.294 | ![](acsos/Context-RobotWorld-2022.07.18.17.47.52.294-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.47.52.949 | ![](acsos/AST-142-2022.07.18.17.47.52.949-parseWait.svg)                             |
| RobotIsReadyToDropToken    |                 | 17.47.53.482 | ![](acsos/AST-143-2022.07.18.17.47.53.482-parseRobotIsReadyToDropToken.svg)          |
| RobotIsReallyReadyToDrop   |                 | 17.47.53.970 | ![](acsos/AST-144-2022.07.18.17.47.53.970-parseRobotIsReallyReadyToDrop.svg)         |
| RobotIsReadyToDrop         |                 | 17.47.54.457 | ![](acsos/AST-145-2022.07.18.17.47.54.457-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.47.54.904 | ![](acsos/AST-146-2022.07.18.17.47.54.904-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.47.55.363 | ![](acsos/AST-147-2022.07.18.17.47.55.363-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.47.55.801 | ![](acsos/AST-148-2022.07.18.17.47.55.801-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.47.56.296 | ![](acsos/AST-149-2022.07.18.17.47.56.296-parseRobotIsReadyToDrop.svg)               |
| RightPlace                 |                 | 17.47.56.817 | ![](acsos/AST-150-2022.07.18.17.47.56.817-parseRightPlace.svg)                       |
| DropObjectAtRightPlace     |                 | 17.47.57.276 | ![](acsos/AST-151-2022.07.18.17.47.57.276-parseDropObjectAtRightPlace.svg)           |
| MoveObjectToCorrectPlace   |                 | 17.47.57.822 | ![](acsos/AST-152-2022.07.18.17.47.57.822-parseMoveObjectToCorrectPlace.svg)         |
| ObjectAtWrongPlace         |                 | 17.47.58.320 | ![](acsos/AST-153-2022.07.18.17.47.58.320-parseObjectAtWrongPlace.svg)               |
| RobotIsNotReadyToPickToken |                 | 17.47.58.833 | ![](acsos/AST-154-2022.07.18.17.47.58.833-parseRobotIsNotReadyToPickToken.svg)       |
| Wait                       |                 | 17.48.07.320 | ![](acsos/AST-155-2022.07.18.17.48.07.320-parseWait.svg)                             |
| RobotIsNotReadyToPickToken |                 | 17.48.07.777 | ![](acsos/AST-156-2022.07.18.17.48.07.777-parseRobotIsNotReadyToPickToken.svg)       |
|                            | object gone     | 17.48.09.459 | ![](acsos/Context-RobotWorld-2022.07.18.17.48.09.459-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.48.16.287 | ![](acsos/AST-157-2022.07.18.17.48.16.287-parseWait.svg)                             |
| RobotIsReadyToPickToken    |                 | 17.48.16.787 | ![](acsos/AST-158-2022.07.18.17.48.16.787-parseRobotIsReadyToPickToken.svg)          |
| RobotIsReallyReadyToPick   |                 | 17.48.17.286 | ![](acsos/AST-159-2022.07.18.17.48.17.286-parseRobotIsReallyReadyToPick.svg)         |
| RobotIsReadyToPick         |                 | 17.48.17.803 | ![](acsos/AST-160-2022.07.18.17.48.17.803-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.48.18.309 | ![](acsos/AST-161-2022.07.18.17.48.18.309-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.48.18.793 | ![](acsos/AST-162-2022.07.18.17.48.18.793-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.48.19.284 | ![](acsos/AST-163-2022.07.18.17.48.19.284-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.48.19.763 | ![](acsos/AST-164-2022.07.18.17.48.19.763-parseRobotIsReadyToPick.svg)               |
| PickUpObject               |                 | 17.48.20.323 | ![](acsos/AST-165-2022.07.18.17.48.20.323-parsePickUpObject.svg)                     |
|                            | object attached | 17.48.20.392 | ![](acsos/Context-RobotWorld-2022.07.18.17.48.20.392-RobotWorld.setTable()-DONE.svg) |
| RobotIsNotReadyToDropToken |                 | 17.48.20.954 | ![](acsos/AST-166-2022.07.18.17.48.20.954-parseRobotIsNotReadyToDropToken.svg)       |
| Wait                       |                 | 17.48.29.505 | ![](acsos/AST-167-2022.07.18.17.48.29.505-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.48.29.956 | ![](acsos/AST-168-2022.07.18.17.48.29.956-parseRobotIsNotReadyToDropToken.svg)       |
|                            | robot idle      | 17.48.35.692 | ![](acsos/Context-RobotWorld-2022.07.18.17.48.35.692-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.48.38.432 | ![](acsos/AST-169-2022.07.18.17.48.38.432-parseWait.svg)                             |
| RobotIsReadyToDropToken    |                 | 17.48.38.984 | ![](acsos/AST-170-2022.07.18.17.48.38.984-parseRobotIsReadyToDropToken.svg)          |
| RobotIsReallyReadyToDrop   |                 | 17.48.39.487 | ![](acsos/AST-171-2022.07.18.17.48.39.487-parseRobotIsReallyReadyToDrop.svg)         |
| RobotIsReadyToDrop         |                 | 17.48.39.995 | ![](acsos/AST-172-2022.07.18.17.48.39.995-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.48.40.646 | ![](acsos/AST-173-2022.07.18.17.48.40.646-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.48.41.455 | ![](acsos/AST-174-2022.07.18.17.48.41.455-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.48.42.167 | ![](acsos/AST-175-2022.07.18.17.48.42.167-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.48.43.036 | ![](acsos/AST-176-2022.07.18.17.48.43.036-parseRobotIsReadyToDrop.svg)               |
| RightPlace                 |                 | 17.48.43.694 | ![](acsos/AST-177-2022.07.18.17.48.43.694-parseRightPlace.svg)                       |
| DropObjectAtRightPlace     |                 | 17.48.44.270 | ![](acsos/AST-178-2022.07.18.17.48.44.270-parseDropObjectAtRightPlace.svg)           |
| MoveObjectToCorrectPlace   |                 | 17.48.45.067 | ![](acsos/AST-179-2022.07.18.17.48.45.067-parseMoveObjectToCorrectPlace.svg)         |
| ObjectAtWrongPlace         |                 | 17.48.45.805 | ![](acsos/AST-180-2022.07.18.17.48.45.805-parseObjectAtWrongPlace.svg)               |
| RobotIsNotReadyToPickToken |                 | 17.48.46.580 | ![](acsos/AST-181-2022.07.18.17.48.46.580-parseRobotIsNotReadyToPickToken.svg)       |
|                            | object gone     | 17.48.55.068 | ![](acsos/Context-RobotWorld-2022.07.18.17.48.55.068-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.48.55.311 | ![](acsos/AST-182-2022.07.18.17.48.55.311-parseWait.svg)                             |
| RobotIsReadyToPickToken    |                 | 17.48.55.877 | ![](acsos/AST-183-2022.07.18.17.48.55.877-parseRobotIsReadyToPickToken.svg)          |
| RobotIsReallyReadyToPick   |                 | 17.48.56.584 | ![](acsos/AST-184-2022.07.18.17.48.56.584-parseRobotIsReallyReadyToPick.svg)         |
| RobotIsReadyToPick         |                 | 17.48.57.159 | ![](acsos/AST-185-2022.07.18.17.48.57.159-parseRobotIsReadyToPick.svg)               |
| RobotIsNotReadyToPick      |                 | 17.48.57.767 | ![](acsos/AST-186-2022.07.18.17.48.57.767-parseRobotIsNotReadyToPick.svg)            |
| RobotIsReadyToPick         |                 | 17.48.58.318 | ![](acsos/AST-187-2022.07.18.17.48.58.318-parseRobotIsReadyToPick.svg)               |
| PickUpObject               |                 | 17.48.58.854 | ![](acsos/AST-188-2022.07.18.17.48.58.854-parsePickUpObject.svg)                     |
|                            | object attached | 17.48.58.861 | ![](acsos/Context-RobotWorld-2022.07.18.17.48.58.861-RobotWorld.setTable()-DONE.svg) |
| RobotIsNotReadyToDropToken |                 | 17.48.59.568 | ![](acsos/AST-189-2022.07.18.17.48.59.568-parseRobotIsNotReadyToDropToken.svg)       |
| Wait                       |                 | 17.49.08.199 | ![](acsos/AST-190-2022.07.18.17.49.08.199-parseWait.svg)                             |
| RobotIsNotReadyToDropToken |                 | 17.49.08.763 | ![](acsos/AST-191-2022.07.18.17.49.08.763-parseRobotIsNotReadyToDropToken.svg)       |
|                            | robot idle      | 17.49.12.098 | ![](acsos/Context-RobotWorld-2022.07.18.17.49.12.098-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.49.17.390 | ![](acsos/AST-192-2022.07.18.17.49.17.390-parseWait.svg)                             |
| RobotIsReadyToDropToken    |                 | 17.49.17.940 | ![](acsos/AST-193-2022.07.18.17.49.17.940-parseRobotIsReadyToDropToken.svg)          |
| RobotIsReallyReadyToDrop   |                 | 17.49.18.601 | ![](acsos/AST-194-2022.07.18.17.49.18.601-parseRobotIsReallyReadyToDrop.svg)         |
| RobotIsReadyToDrop         |                 | 17.49.19.185 | ![](acsos/AST-195-2022.07.18.17.49.19.185-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.49.19.787 | ![](acsos/AST-196-2022.07.18.17.49.19.787-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.49.20.367 | ![](acsos/AST-197-2022.07.18.17.49.20.367-parseRobotIsReadyToDrop.svg)               |
| RobotIsNotReadyToDrop      |                 | 17.49.20.988 | ![](acsos/AST-198-2022.07.18.17.49.20.988-parseRobotIsNotReadyToDrop.svg)            |
| RobotIsReadyToDrop         |                 | 17.49.21.715 | ![](acsos/AST-199-2022.07.18.17.49.21.715-parseRobotIsReadyToDrop.svg)               |
| RightPlace                 |                 | 17.49.22.301 | ![](acsos/AST-200-2022.07.18.17.49.22.301-parseRightPlace.svg)                       |
| DropObjectAtRightPlace     |                 | 17.49.22.924 | ![](acsos/AST-201-2022.07.18.17.49.22.924-parseDropObjectAtRightPlace.svg)           |
| MoveObjectToCorrectPlace   |                 | 17.49.23.638 | ![](acsos/AST-202-2022.07.18.17.49.23.638-parseMoveObjectToCorrectPlace.svg)         |
| NotEmptyTable              |                 | 17.49.24.549 | ![](acsos/AST-203-2022.07.18.17.49.24.549-parseNotEmptyTable.svg)                    |
| Wait                       |                 | 17.49.33.854 | ![](acsos/AST-204-2022.07.18.17.49.33.854-parseWait.svg)                             |
| WaitForEmptyTable          |                 | 17.49.34.525 | ![](acsos/AST-205-2022.07.18.17.49.34.525-parseWaitForEmptyTable.svg)                |
| NotEmptyTable              |                 | 17.49.35.242 | ![](acsos/AST-206-2022.07.18.17.49.35.242-parseNotEmptyTable.svg)                    |
|                            | object gone     | 17.49.37.572 | ![](acsos/Context-RobotWorld-2022.07.18.17.49.37.572-RobotWorld.setTable()-DONE.svg) |
| Wait                       |                 | 17.49.43.910 | ![](acsos/AST-207-2022.07.18.17.49.43.910-parseWait.svg)                             |
| WaitForEmptyTable          |                 | 17.49.44.712 | ![](acsos/AST-208-2022.07.18.17.49.44.712-parseWaitForEmptyTable.svg)                |
| EmptyTable                 |                 | 17.49.45.507 | ![](acsos/AST-209-2022.07.18.17.49.45.507-parseEmptyTable.svg)                       |
| Tidy                       |                 | 17.49.46.201 | ![](acsos/AST-210-2022.07.18.17.49.46.201-parseTidy.svg)                             |
|                            | robot busy      | 17.49.46.244 | ![](acsos/Context-RobotWorld-2022.07.18.17.49.46.244-RobotWorld.setTable()-DONE.svg) |
|                            |                 | 17.49.46.867 | ![](acsos/AST-211-2022.07.18.17.49.46.867-complete.svg)                              |
|                            | robot idle      | 17.49.56.229 | ![](acsos/Context-RobotWorld-2022.07.18.17.49.56.229-RobotWorld.setTable()-DONE.svg) |
