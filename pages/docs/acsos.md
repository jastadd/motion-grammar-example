# The Robot Sorting Grammar

## Publication

A version of this use case was published as a demo at [ACSOS 2022](https://2022.acsos.org/).

J. Mey, R. Schöne, A. Podlubne and U. Aßmann, "Specifying Reactive Robotic Applications With Reference Attribute Motion Grammars," 2022 IEEE International Conference on Autonomic Computing and Self-Organizing Systems Companion (ACSOS-C), 2022, pp. 72-73, doi: [10.1109/ACSOSC56246.2022.00035](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9935078&isnumber=9934873).

**Abstract:** *There are several changes when describing the behavior of cyber-physical systems, especially if they act in a complex environment. First, the description should be easily comprehensible to avoid unwanted and unexpected behavior. Second, both the description of the behavior and the description and analysis of the context should be expressive enough to allow complex behavior. Motion grammars have previously been presented as a means to describe high-and low-level behavior of a reactive cyber-physical system. This work proposes Reference Attribute Grammars not only to implement and extend the concept of motion grammars but also to describe and analyze the context. This demonstrator shows how the resulting concept of reference attribute motion grammars is used to control a pick-and-place task while reacting to a changing environment.*


## Video

![type:video](https://www.youtube.com/embed/KBuFVsKjA1Q)
